#przekazywanie wartosci

#przekazywanie listy do funkcji

def greetUsers(Names):
    """Powitanie userów z listy przesłanej do funkcji"""
    for Name in Names:
        msg="Witaj, " + Name.title() + "!"
        print(msg)
userNames = ['ala', 'ola', 'tomek', 'pawel']
greetUsers(userNames)

#modyfikowanie listy w funkcji

def printModels(unPrintedDesigns, completedModels):
    """Sylumalacja wydruku i przeniesienie pomiedzy listami """
    while unPrintedDesigns:
        currentDesign = unPrintedDesigns.pop()
        print("Wydruk modelu: " + currentDesign)
        completedModels.append(currentDesign)

def showCompletedModels(completedModels):
    """Wyświetlanie wszystkich modeli"""
    print("Wydrukowane zostały następujące modele: ")
    for completedModel in completedModels:
        print(completedModel)

unPrintedDesigns = ['iphone case', 'robot pedant', 'dodecahadron']
completedModels = []
printModels(unPrintedDesigns,completedModels)
showCompletedModels(completedModels)

#przekazywanie dowolnej ilości atgumentów do funckji

def makePizza(*toppings): # * - tworzenie krotki do przechowywania argumentów
    """Wyświetlanie dodatków wybrsaych przez klienta"""
    print(toppings)
makePizza('pepperoni')
makePizza('pepperoni','pieczrki','ser')

# argumenty pozycyjne i elementy dodatkowe

def makePizza(size, *toppings): # * - tworzenie krotki do przechowywania argumentów
    """Wyświetlanie dodatków wybrsaych przez klienta"""
    print("Przygotowuję pizzę o wielkości: " + str(size) + " cm z dodatkami: ")
    for topping in toppings:
        print(" - " + topping)

    print(toppings)
makePizza(30, 'pepperoni')
makePizza(40, 'pepperoni','pieczrki','ser')

# dowolna ilśc argumentów w postaci słów kluczowych
def buildProfile(first, last, **userInfo):#definicja pustego słownika
    """Budowa słownika zawierającego informacje o profilu użytkownika"""
    Profile = {}
    Profile['firstName'] = first
    Profile['lastName'] = last
    for key, value in userInfo.items():
        Profile[key] = value
    return Profile
userProfile = buildProfile('albert', 'einstein',
                           location = 'princetown',
                           field = 'fizyka')
print(userProfile)

