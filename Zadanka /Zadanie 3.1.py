#Napisz program sprawdzający czy w 10 - elementowej tablicy całkowitej
# zawierającej liczby losowe z przedziału (5, 41>, wszystkie elementy
# są podzielne przez 3 (liczby losowe powinny być generowane w programie).

import random
liczbyWylosowane=[]
liczbyPodzielne=[]
losowaLiczba = 0

for liczba in range(0,10):
    losowaLiczba = random.randint(6, 41)
    liczbyWylosowane.append(losowaLiczba)

for podzielna in liczbyWylosowane:
    if(podzielna%3 == 0):
        liczbyPodzielne.append(podzielna)


print("Wylosowane liczby to: " + str(liczbyWylosowane))
print("Liczby podzielne przez 3 to: " + str(liczbyPodzielne))

if(len(liczbyPodzielne) == 10):
    print("Wszystkie elementy w tablicy są podzielne")
else:
    print("Ilosc liczb podzielnych przez 3: " + str(len(liczbyPodzielne)))






