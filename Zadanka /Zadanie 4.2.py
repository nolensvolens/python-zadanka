class User():
    def __init__(self, firstName, lastName, age, favColor, country, city, street, number):
        self.firstName = firstName
        self.lastName = lastName
        self.age = age
        self.favColor = favColor
        self.country = country
        self.city = city
        self.street = street
        self.number = number
        self.login = 0;

    def greetUser(self):
        print("Witaj " + self.firstName + ' ' + self.lastName+ "!")

    def describeUser(self):
        print("Twoj wiek to " + str(self.age))
        print("Twoj ulubiony kolor to: " + self.favColor)
        print("Twoje miejsce zamieszkania to: " + self.street + ' ' + str(self.number) + ' ' + self.city + ' ' + self.country)

    def loginAttempts(self):
        self.login +=1
        #print(self.login)

    def clearAttempts(self):
        self.login = 0
        print("Wyczyszczono proby logowania")


newUser = User('Adam', 'Kowalski', 19, 'niebieski', 'Polska', 'Warszawa', 'Poziomkowa', 20)
"""newUser.greetUser()
newUser.describeUser()

newUser = User('Kasia', 'Nowak', 29, 'czerwony', 'Polska', 'Gdansk', 'Zielona', 78)
newUser.greetUser()
newUser.describeUser()"""


for i in range(10):
    newUser.loginAttempts()

print("Ilosc prob logowania: " + str(newUser.login))

newUser.clearAttempts()

print("Po czyszczeniu: " + str(newUser.login))

