#Napisz programy, który zamieni na system binarny, oktalny i heksadecymalny liczby podane
#  na liście decNumbers[523, 458, 399, 878, 1001, 1112, 2056]. Zamienione wartości liczb zapisz na osobnych listach
# binNumbers[ ] dla systemu binarnego, octNumbers[ ] dla systemu ósemkowego i hexNumbers[ ] dla systemu szesnastkowego.

 

decNumbers = [523, 458, 399, 878, 1001, 1112, 2056]
binNumbers = []
octNumbers = []
hexNumbers = []

for number in range(len(decNumbers)):
    binNumbers.append(bin(decNumbers[number]))
    octNumbers.append(oct(decNumbers[number]))
    hexNumbers.append(hex(decNumbers[number]))

print("Po konwersji na binarny: ")
print(binNumbers)
print("Po konwersji na osemkowy: ")
print(octNumbers)
print("Po konwersji na heksadecymalny: ")
print(hexNumbers)