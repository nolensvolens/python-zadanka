#rekurencja

#wyznaczanie elementów ciągu Fibonacciego
def Fibonacci(Element):
    """Wyznaczanie elementów ciągu Fibonacciego"""
    if Element < 2:
        return Element
    else:
        return Fibonacci(Element - 1) + Fibonacci(Element - 2)
Element = int(input("Podaj element ciagu Fibonacciego który mam  obliczyć (liczymy od 0)! : "))
print("Element ciagu Fibonacciego to: " + str(Fibonacci(Element)))

