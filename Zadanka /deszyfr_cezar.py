ALFABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
ZASZYFROWANY_TEKST = "SF SFZPJ SNLID SNJ OJXY EF UTEST"
KLUCZ = 5

def deszyfruj(tekst):
    odszyfrowany = ""
    KLUCZM = KLUCZ % 26
    for znak in tekst:
        if (ord(tekst) - KLUCZM < 97):
            odszyfrowany += chr(ord(tekst) - KLUCZM + 26)
        else:
            odszyfrowany += chr(ord(tekst) - KLUCZM)
    return odszyfrowany


print(deszyfruj(ZASZYFROWANY_TEKST))

