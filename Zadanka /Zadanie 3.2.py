#Napisz program wpisujący do tablicy jednowymiarowej n liczb
# całkowitych wygenerowanych losowo z przedziału (0, 100) i
# wyznaczający maksymalny element w tej tablicy oraz określający
# liczbę jego wystąpień.
import random

dlugoscTablicy = int(input("Podaj dlugosc tablicy: "))
tablicaLiczb = []
takieSame =[]
losowaLiczba = 0
iloscMaksow = 0


for liczba in range(dlugoscTablicy):
    losowaLiczba = random.randint(1, 99)
    tablicaLiczb.append(losowaLiczba)

def znajdzMinimum(tablica):
    for liczba in range(len(tablica)):

        maxLiczba = tablica[liczba]
        liczba2 = 1
        dlugoscTab = len(tablica)

        if(liczba2 < dlugoscTab - 1):
            if(tablica[liczba2] > maxLiczba):
                maxLiczba = tablica[liczba2]
        else:
            liczba2 += 1
    return maxLiczba



maksymalnaLiczba = znajdzMinimum(tablicaLiczb)

for liczba in tablicaLiczb:
    if(maksymalnaLiczba == liczba):
        takieSame.append(liczba)


iloscMaksow = len(takieSame)


print("Maksymalna liczba to: " + str(maksymalnaLiczba))
print("Liczba powtorzen maksymalnej liczby to: " + str(iloscMaksow))
print("Tablica liczb to: " + str(tablicaLiczb))
