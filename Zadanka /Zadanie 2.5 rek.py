#Napisz programy iteracyjnie i rekurencyjnie, które wypiszą na
# standardowym wyjściu 20 pierwszych elementów ciągu Fibonacciego.

print("Rekurencyjnie: ")

def Fibonacci(Element):
    if Element < 2:
        return Element
    else:
        return Fibonacci(Element - 1) + Fibonacci(Element - 2)

for x in range(21):
    print("Dla " + str(x) +  " to: " + str(Fibonacci(x)))
