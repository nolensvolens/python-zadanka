#ZuzannaDąbrowa
#Napisz program konwertujący tekst wpisywany z klawiatury na zapis w alfabecie Morse'a oraz program tłumaczący zapis w
#alfabecie Morse'a na zwykły tekst.
charTab = []
deCoded = ''
code = {'A': '.-', 'B': '-...', 'C': '-.-.',
        'D': '-..', 'E': '.', 'F': '..-.',
        'G': '--.', 'H': '....', 'I': '..',
        'J': '.---', 'K': '-.-', 'L': '.-..',
        'M': '--', 'N': '-.', 'O': '---',
        'P': '.--.', 'Q': '--.-', 'R': '.-.',
        'S': '...', 'T': '-', 'U': '..-',
        'V': '...-', 'W': '.--', 'X': '-..-',
        'Y': '-.--', 'Z': '--..', ' ': '/'
        }
deCode = dict((v, k) for (k,v) in code.items())
choice = int(input("Wbierz: 1.Kodowanie/2.Odkodowanie w morsie"))
if (choice == 1):
    toCode = input('ZDANIE:  ')
    for char in toCode:
        charTab.append(code[char.upper()] + ";")
    print("Słowa oddzielane znakiem ';'")
    print("Zakodowane:  " + str(''.join(charTab)))
elif(choice== 2):
    toUncode = input("Wpisz zdanie do odkodowania(litery oddziel znakiem ';': ")
    toUncode = toUncode.split(';')
    for i in range(len(toUncode)):
        deCoded += deCode[toUncode[i]]
    print(deCoded)
        
else:
    print(" Coś wpisano źle :((!(Może nieprawidłowy znak?)")

