#programowanie obiektowe
#tworzenie klasy
class Dog():
    """Klasa opisujaca dowolnego psa"""

    def __init__(self, name, age):
        """inicjalizacja atrybutow name i age"""
        self.name = name
        self.age = age

    def sit(self):
        """symulacja siadania psa na komende"""
        print(self.name.title() + "teraz siedzi.")

    def givePaw(self):
        """symulacja dawania łapy przez komende"""
        print(self.name.title() + "teraz podaje łape")


#egzemplarz klasy
myDog = Dog("blop", 5)

print("Moj pies ma na imie" + myDog.name.title() + ".")
print("Moj pies ma " + str(myDog.age) + "lat.")
myDog.sit()
myDog.givePaw()


class Human():
    def __init__(self, name, age, color):
        self.name = name
        self.age = age
        self.color = color

    def hname(self):
        print("Imie: " + self.name.title())

    def hage(self):
        print("Wiek: " + str(self.age))

    def hcolor(self):
        print("Ulubiony kolor: " + self.color.title())


myHuman = Human("Zosia", 18, "Zielony")

myHuman.hname()
myHuman.hcolor()
myHuman.hage()


#praca z klasami

class Car():
    """Klasa reprezentujaca samochod"""

    def __init__(self, brand, model, year):
        self.brand = brand
        self.model = model
        self.year = year
        self.odometer = 0

    def descriptiveName(self):
        longName = str(self.year) + " " + self.brand + " " + self.model
        return longName.title()

    def readOdometer(self):
        """Informacja o przebiegu"""
        print("Ten samochod ma przejechane " + str(self.odometer) + " km.")

    def updateOdometer(self, valueOdometer):
        if valueOdometer >= self.odometer:
             self.odometer = valueOdometer
        else:
            print("Nie mozna cofnac licznika")


newCar = Car('audi', 'a4', 2016)
print(newCar.descriptiveName())
newCar.updateOdometer(3200000)
newCar.updateOdometer(1200000)
newCar.readOdometer()


