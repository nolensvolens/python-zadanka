#Napisz program obliczający rekurencyjnie wartość wielomianu stopnia n z wykorzystaniem schematu Hornera,
#  zgodny z podaną poniżej specyfikacją - Horner rekurencyjnie.

 

def horner(wspolczynniki, stopien, arg):
    if(stopien == 0):
        return wspolczynniki[0]
    return arg * horner(wspolczynniki, stopien-1, arg) + wspolczynniki[stopien]


wspolczynniki = []
stopien = int(input("Podaj stopien wielomianu: "))
temp = stopien

for i in range(stopien+1):
    wspolczynniki.append(None)


for j in range(len(wspolczynniki)):
    wspolczynnik = int(input("Podaj wspolczynnik przy stopniu " + str(temp)+ ": "))
    wspolczynniki[j] = wspolczynnik
    temp = temp - 1

arg = int(input("Podaj wartosc argumentu x: "))

print(horner(wspolczynniki, stopien, arg))

