# program, który w sposób rekurencyjny wypisze wartość silni dla
# liczb z przedziału od 0 do 10.

def Silnia(Element):
    if(Element  < 2):
        return 1
    else:
        return Element*Silnia(Element-1)


for x in range(11):
    print("Silnia z " + str(x) + " jest równa: " + str(Silnia(x)))